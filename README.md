Non-inverting high-side switch
==============================

![KiCad rendering of board](p-driver-render.png)

This is a board designed to be easy to populate to boost voltage
and/or current capacity for low-voltage and/or low-current GPIO
signals for common-ground applications, where for any reasons
a simple low-side switch is not feasible.  It is meant to be
sufficiently flexible to use as an active output level shifter
(up to 1A/channel, 3A total), as well as a high current (up to
5A/channel, 10A total) high-side switch, with up to 8 channels.

Two sets of headers are supplied for input and output.  The
2.54mm / 0.1" pitch are intended for pin blocks, mini screw
terminals, or other similar styles.  The 5.08 / 0.2" pitch
are intended for eurostyle screw terminal blocks such as Molex
or TE.

Maximum design currents (max 30⁰C rise):
 *  VDD: 10A on 5.08mm headers, 3A on 2.54mm headers
 *  VCC input: 10A on 5.08mm headers, 3A on 2.54mm headers
 *  VCC output: 5A on 5.08mm headers, 3A on 2.54mm headers
 *  Signal inputs: negligable current
 *  Signal outpus: 5A on 5.08mm headers, 1A on 2.54mm headers

Appropriate fuses and voltage regulators must be chosen to support
the load.

The n-MOSFETs can be any GSD SOT-23-3 similar to the PMV20XNE
PMV30UN2, or MMBF170 that has sufficiently low RDS(on) at the
required input voltage for the application.  Current is negligable;
the n-MOSFETs are only inverting drivers for the p-MOSFETs.

The p-MOSFETs can be any GSD SOT-23-3 similar to the PMV33UPE
with sufficient current capacity for the application.

VCC may be supplied directly, or one of the voltage regulators
may be populated to supply VCC from VDD.  A TO-220 switching
regulator package may have its own filtering capacitors, in
which case the capacitors do not need to be populated on this
board.  A SOT-23-3 regulator such as the MCP1702T-5002E/CB
will require that the filtering capacitors be installed. The
values of 1uF on input and 4.7uF on output are as specified for
the MCP1702T-5002E/CB; check the datasheet for the regulator
you use and make sure that the footprint matches the schematic.
Make sure that the voltage regulator you select can handle
VDD max voltage.

Pads for two common sizes of polyfuse are provided.  You can probably
make the pads work with the appropriate fuse for your application.

Channels may be individually populated as needed. R1/R5 and
other current-limiting resistors may be jumpered 0R if not
needed for an application. Pull-down and pull-up resistor
values may be chosen; typical values are supplied but not
necessarily appropriate for all cases.  For high output
current, current limiting output resistors (e.g. R5) will
normally not be needed, and the jumpers will need to be
sufficient for the current being drawn.

Populate surface mount components before through-hole
components.  The vertical resistor arrangement saves board
space but makes the MOSFETs less convenient to access after
the resistors are installed.


Copyright and License
=====================

Copyright Michael K Johnson

Licensed under CERN OHL v.1.2

This documentation describes Open Hardware and is licensed under the
CERN OHL v. 1.2.

You may redistribute and modify this documentation under the terms
of the CERN OHL v.1.2. (http://ohwr.org/cernohl). This documentation
is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF
MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR
PURPOSE. Please see the CERN OHL v.1.2 for applicable conditions
